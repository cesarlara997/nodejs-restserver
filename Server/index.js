require('./config.js')
const express = require('express');
const app = express();
const bodyParser= require('body-parser');
require('morgan');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 

// parse application/json

app.use(bodyParser.json());
app.get('/',(req,res)=>{
res.send('Hello world')

});

app.listen(process.env.PORT,()=>{
console.log('LISTEN PORT:'+process.env.PORT);

});